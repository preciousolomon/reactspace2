import React, {useState, useEffect} from 'react';

import './index.css';
import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';
import { Loading } from './globalcomponents';

const Register = React.lazy(() => import('./views/Users/Register'));
const Login = React.lazy(() => import('./views/Users/Login'));
const Home = React.lazy(() => import('./views/Layouts/Home'));
const Rooms = React.lazy(() => import('./views/Rooms/Rooms'));
const Bookings = React.lazy(() => import('./views/Booking/Bookings'));
const Users = React.lazy(() => import('./views/Users/Users'));


const App = () => {

const [isLoading, setIsLoading] = useState(true);
const [user, setUser] = useState({})
const [isAdmin, setIsAdmin] = useState(false)

useEffect(() => {
  if (sessionStorage.token) {
    let user = JSON.parse(sessionStorage.user)
    setUser(user);
    setIsAdmin(user.isAdmin);
    setIsLoading(false)
  } else {
    console.log(user)
    setIsLoading(false)
  }
},[])
  return (
    <React.Fragment>
      {isLoading ?
        <Loading />
        :
        <HashRouter>
          <React.Suspense fallback={<Loading />}>
            <Switch>
              <Route
                path='/'
                exact
                name="Home"
                render={props =>
                  <Home {...props} />
                }
              >
              </Route>
              <Route
                path='/showbookings'
                exact
                name="Home"
                render={props =>
                  <Bookings {...props} />
                }
              >
              </Route>
              <Route
                path='/register'
                exact
                name="Register"
                render={props =>
                  <Register {...props} />
                }
              >
              </Route>
              <Route
                path='/login'
                exact
                name="Login"
                render={props =>
                  <Login {...props} />
                }
              >
              </Route>
              <Route
                path='/rooms'
                exact
                name="Rooms"
                render={props =>
                  <Rooms {...props} />
                }
              >
              </Route>
              <Route
                path='/showUsers'
                exact
                name="Users"
                render={props =>
                  <Users {...props} />
                }
              >
              </Route>
              <Route
                path='/home'
                exact
                name="Home"
                render={props =>
                  <Home {...props} />
                }
              >
              </Route>

            </Switch>
          </React.Suspense>
        </HashRouter>
      }
    </React.Fragment>
  )
};
export default App;
