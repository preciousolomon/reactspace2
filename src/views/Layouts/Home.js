import React from 'react';
import {Footer, NavigationBar}  from './index';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';


const Home = () => {
    return (
      <React.Fragment>
        <NavigationBar />
        
        <div className="container-standard">
          <h2 className="text-left"><br/><br/>Great Spaces.</h2>
          <h2 className="text-left my-3">Great Atmosphere.</h2>
          <div className="motivation-text">
            <br/>
            <h4 className="container-text">At ReactSpaces, we're all about re-imagined spaces that elevate the whole working experience.</h4>
            <h4 className="container-text">Whether you need a workspace fot just a few times a week or a space to play with your friends. </h4>
            <br />
            <br/>
            <h4 className="container-text">Discover spaces to rent </h4>
          </div>
          <Link to='/rooms'><Button>View Spaces</Button></Link>
          <br />
          <br/>
        </div>
        <br />
        <br />
        <br />
       
       <div className="content-block home-container py-5">
      <div className="container home-container">
          <div className="row align-items-center">
              <div className="col-md-6">
                  <img className="shadowed border-rad-8" src="https://images.pexels.com/photos/374857/pexels-photo-374857.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="PlaySpace"></img> 
              </div>
              <div className="col-md-6 home-container">
                  <h2 className="heads">WorkSpace</h2>
                 <p className="footnote">Book a shared work space in a our coworking space in Ortigas. Choose an open seat or reserve a dedicated seat just for you.</p>
        
                  <Link to='/rooms'><Button>Go to Space Rentals</Button></Link>
                  <p className="footnote">
                     *First come, first served. Come early to secure your favorite spot.</p>
                  
              </div>
          </div>
          <div className="row">
              <div className="col-12 footnote">
                  <p>
                     Located at the penthouse floor of BSA Twin Towers, our coworking space is perfect for freelancers, entrepreneurs and professionals who like to work in a space with a relaxed, creative vibe. 
              </p>
              </div>
          </div>
      </div>
    </div>
    <div className="row">
            <img className="collage" src="https://images.pexels.com/photos/3194523/pexels-photo-3194523.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt=""/>
            <img className="collage" src="https://images.pexels.com/photos/2789274/pexels-photo-2789274.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />    
            <img className="collage" src="https://images.pexels.com/photos/1100538/pexels-photo-1100538.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" /> 
        </div>

<div className="content-block home-container py-5">
<div className="container home-container">
<div className="row align-items-center">
  <div className="col-md-6 home-container">
        <h2 className="heads">Virtual Space</h2>
        <p className="footnote"> We've designed fully-equipped meeting rooms that will turn your routine team meeting into a dynamic exchange of ideas. We want your meetings to be productive and far from dull and boring.</p>
        

        <Link to='/rooms'><Button>Go to Space Rentals</Button></Link>
        <p className="footnote">*First come, first served. </p>
  </div>
  <div className="col-md-6">
       <img className="shadowed border-rad-8 content-img" src="https://images.pexels.com/photos/37347/office-sitting-room-executive-sitting.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="RemoteSpace"></img>
  </div>
</div>
<div className="row">
  <div className="col-12 footnote">
        <p>
            Located at the penthouse floor of BSA Twin Towers, our remote office space comes with mail parcel handling.
        </p>
  </div>
</div>   
</div>
        </div> 
         <div className="row">
            <img className="collage" src="https://images.pexels.com/photos/3194521/pexels-photo-3194521.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />
            <img className="collage" src="https://images.pexels.com/photos/65781/pexels-photo-65781.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />   
            <img className="collage" src="https://images.pexels.com/photos/1543895/pexels-photo-1543895.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />
        </div>
    <div className="content-block home-container py-5">
      <div className="container home-container">
          <div className="row align-items-center">
              <div className="col-md-6">
                  <img className="shadowed border-rad-8" src="https://images.pexels.com/photos/374857/pexels-photo-374857.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="PlaySpace"></img> 
              </div>
              <div className="col-md-6 home-container">
                  <h2 className="heads">PlaySpace</h2>
                 <p className="footnote">Book a Living Room for 6 hours. For times where you need to relax and unwind. The place comes with an integrated sound system, furniture and a playstation 4 to relax with your friends, make a project and unwind.</p>
        
                  <Link to='/rooms'><Button>Go to Space Rentals</Button></Link>
                  <p className="footnote">
                     *First come, first served. Come early to secure your favorite spot.</p>
                  
              </div>
          </div>
          <div className="row">
              <div className="col-12 footnote">
                  <p>
                    Located at the penthouse floor of BSA Twin Towers, our living space is perfect for groups who want to have an intimate party, private hangout away from the noise of the city.
                  </p>
              </div>
          </div>
      </div>
    </div>
         <div className="row">
            <img className="collage" src="https://images.pexels.com/photos/2312369/pexels-photo-2312369.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />
            <img className="collage" src="https://images.pexels.com/photos/3184430/pexels-photo-3184430.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="" />   
            <img className="collage" src="https://images.pexels.com/photos/1346197/pexels-photo-1346197.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt=""/>      
        </div>
    <div className="content-block home-container py-5">
      <div className="container home-container">
          <div className="row align-items-center">
              <div className="col-md-6 home-container">
                    <h2 className="heads">GroupSpace</h2>
                    <p className="footnote"> We've designed fully-equipped meeting rooms that will turn your routine team meeting into a dynamic exchange of ideas. We want your meetings to be productive and far from dull and boring.</p>
                    
          
                    <Link to='/rooms'><Button>Go to Space Rentals</Button></Link>
                    <p className="footnote">*First come, first served. </p>
              </div>
              <div className="col-md-6">
                    <img className="shadowed border-rad-8 content-img" src="https://images.pexels.com/photos/3183150/pexels-photo-3183150.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="GroupSpace"></img>
              </div>
          </div>
          <div className="row">
              <div className="col-12 footnote">
                    <p>
                        Located at the penthouse floor of BSA Twin Towers, our remote office space comes with mail parcel handling.
                    </p>
              </div>
          </div>   
        </div>
      </div> 
     
    <Footer />                              
    </React.Fragment>
    )
}
export default Home;