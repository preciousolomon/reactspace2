import React from 'react';

const Footer = () => {
    return(
        <React.Fragment >
         
            <div className="footer-custom mt-3 col-lg-12">
                <p className="text-center header-text">
                    <strong>Created By: Precious Solomon</strong>
                </p>
            </div>
       
        </React.Fragment>
            
    )
}
export default Footer;