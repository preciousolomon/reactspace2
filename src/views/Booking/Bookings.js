import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {BookList} from './components'
import { NavigationBar, Footer } from '../Layouts'
import CsvDownload from 'react-json-to-csv'



const Bookings = () => {
	const [bookings, setBookings] = useState([])
	const [user, setUser] = useState({})
	const [userData, setUserData] = useState([])
	

	useEffect(()=>{

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			if(user.isAdmin==="true"){
				fetch('https://damp-citadel-30770.herokuapp.com/showbookings')
					.then(response => response.json())
					.then(data => {
						setBookings(data)
					})
					setUser(user)
			}else{
				axios.get('https://damp-citadel-30770.herokuapp.com/showbookingsbyuser/'+user.id).then(res=>{
					setBookings(res.data)
				})
			}
			setUser(user);

			axios.get('https://damp-citadel-30770.herokuapp.com/showusers').then(res => {
				console.log(res.data)
				setUserData(res.data)
			})
		}else{
			window.location.replace('#/login')
		}

	}, []);

	const handleDeleteBooking = (bookingId) => {
		axios.delete('https://damp-citadel-30770.herokuapp.com/deletebooking/'+bookingId).then(res=>{
			let index = bookings.findIndex(booking=>booking._id===bookingId);
			let newBookings = [...bookings];
			newBookings.splice(index,1);
			setBookings(newBookings);
		})
	}

	return (
		<React.Fragment>
			<NavigationBar />
			{bookings.length == 0 ?
				<h1 className="text-center py-5 page-header">No Booking!</h1>
				: 
				<React.Fragment>
				<div className="page-header d-flex justify-content-center align-items-center">
					<h1 className="header-text">My Bookings</h1>
				</div>
				
					<table
						className="table table-responsive col-lg-10 offset-lg-1 bg-dark text-white text-center"
					>
						<thead>
							<tr>
								
								<th>Booking Code:</th>
								{user.isAdmin === "true" ?
								<th>Booked By: </th>
											: ""}
								<th>Booked On:</th>
								<th>Start Date:</th>
								<th>End Date:</th>
								<th>Booking Type:</th>
								<th>No. of Days</th>
								<th>Total Amount:</th>
								
								<th>
								<CsvDownload className="text-center btn btn-success" data={bookings} />
								</th>
							</tr>
						</thead>
						<tbody>
			
						{bookings.map(booking => (
								<BookList
									key={booking._id}
									booking={booking}
									handleDeleteBooking={handleDeleteBooking}
									user={user}
									userData={userData}
								
								/>
							))}			
						</tbody>
					</table>
				<Footer />	
			</React.Fragment>
			}
			
		</React.Fragment>
	)
}
export default Bookings;