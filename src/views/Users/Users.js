import React, {useEffect, useState} from 'react';
import axios from 'axios'
import {UserList} from './components'
import { NavigationBar, Footer } from '../Layouts'
import CsvDownload from 'react-json-to-csv'


const Users = () => {
        
	const [Users, setUsers] = useState([])
	
	

	useEffect(()=>{

		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			if(user.isAdmin==="true"){
				fetch('https://damp-citadel-30770.herokuapp.com/showusers')
					.then(response => response.json())
					.then(data => {
						setUsers(data)
					})
					
			}else{
				axios.get('https://damp-citadel-30770.herokuapp.com/showusers').then(res => {
				console.log(res.data)
				
			})
			}
			

			
		}else{
			window.location.replace('#/login')
		}

	}, []);

    const handleDeleteUser = (UserId) => {
        axios.delete('https://damp-citadel-30770.herokuapp.com/deleteUser/' + UserId).then(res => {
            let index = Users.findIndex(User => User._id === UserId);
            let newUsers = [...Users];
            newUsers.splice(index, 1);
            setUsers(newUsers);
        })
    };
 
    
    const handleMakeUserAdmin = (UserId) => {
        axios.patch('https://damp-citadel-30770.herokuapp.com/updateadminstatus/' + UserId).then(res => {
            let index = Users.findIndex(User => User._id === UserId);
            let newUsers = [...Users];
            newUsers.splice(index, 1, res.data);
            setUsers(newUsers);
        })
    };
            
       
	return (
		<React.Fragment>
			<NavigationBar />
			<br/>
				<div className="page-head">
					<h1
						className="text-center py-5 page-header">User Record</h1>

				
				<table
					className="table table-dark col-lg-10 offset-lg-1 bg-dark text-white text-center"
				>
					<thead>
						<tr>
					
				        <th> First Name:</th>
                        <th> Last Name:</th>
                        <th> Email: </th>  
				        <th> Admin Status:</th>
							
							
							<th>
							<CsvDownload className="text-center btn btn-success" data={Users} />
							</th>
						</tr>
					</thead>
					<tbody>
		
					{Users.map(User => (
							<UserList
								key={User._id}
								User={User}
                            handleDeleteUser={handleDeleteUser}
                            handleMakeUserAdmin={handleMakeUserAdmin}
							
							/>
						))}			
					</tbody>
				</table>
				
			<Footer />
		</div>
				
	</React.Fragment>
	)
}
export default Users;