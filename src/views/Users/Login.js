import React, {useState} from 'react';
import { FormInput } from '../../globalcomponents';
import { Button } from 'reactstrap';
import axios from 'axios';
import { NavigationBar, Footer} from '../Layouts';
import { Link } from 'react-router-dom';



const Login = () => {

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const handleEmailChange = (e) => {
        setEmail(e.target.value)
        console.log(e.target.value)
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
        console.log(e.target.value)
    }

    const handleLogin = () => {
        axios.post('https://damp-citadel-30770.herokuapp.com/login', {
            email,
            password
        }).then(res => {
            //save only as string
            sessionStorage.token = res.data.token;
            //converts data into string
            sessionStorage.user = JSON.stringify(res.data.user);
            console.log(res.data.token);
            window.location.replace('#/home');
        })
    }
    return (
        <React.Fragment>
            
            <NavigationBar /> 
            <br/>
            <br />
            
            <div className="col-lg-4 offset-lg-4 border container-fluid">
                <h1 className="text-center text-strong text-dark pt-5">Login</h1>
                
                <FormInput
                    label={"Email"}
                    placeholder={"Enter your email"}
                    type={"email"}
                    onChange={handleEmailChange}
                    
                />
                <FormInput
                    label={"Password"}
                    placeholder={"Enter your password"}
                    type={"password"}
                    onChange={handlePasswordChange}
                />
                <Button
                    block
                    color="info"
                    onClick={handleLogin}
                    
                > Login
                <br/>
                </Button>
                <p><Link to="/register">Not yet a user?</Link></p>
                </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <Footer />
        </React.Fragment>
    );
    
};

export default Login;