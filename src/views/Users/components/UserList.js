import React from 'react';
import {
	Button
} from 'reactstrap'




const UserList = props => {



	const user = props.User;

	
	return (
		<React.Fragment>
			<tr className="text-center">
				<td> {user.firstName}</td>
				<td> {user.lastName}</td>
				<td> {user.email}</td>
				<td> {user.isAdmin === "false" ?
					<Button
						color="secondary"
						className="col-lg-12"
						onClick={() => props.handleMakeUserAdmin(user._id)}>
						Make Admin</Button>
							 
					
					:
					"Admin"} 
				</td>
			
				<td>
					<Button
						color="danger"
						className="col-lg-12"
						onClick={()=>props.handleDeleteUser(user._id)}>
						Delete User
					</Button>
				</td>
				<td>
					
				</td>			
			</tr>				
							
		</React.Fragment>
	)
}

export default UserList;